<?php
/**
 * @file
 * Advanced forum maintenance module
 */


/**
 * Implements hook_menu().
 * Provide a URL for the form to sit on so we can put it in the menu as well
 * providing a
 */
function advanced_forum_maintenance_menu() {

    $items = array();

    // Add the actual menu item - requires the misc module.
    $items['admin/config/development/forum-maintenance'] = array(
      'title'            => 'Advanced forum maintenance settings',
      'description'      => 'Manage the access to the advance forum when in maintenance',
      'page callback'    => 'drupal_get_form',
      'page arguments'   => array('advanced_forum_maintenance_admin_form'),
      'file'             => 'includes/admin_settings_form.inc',
      'access arguments' => TRUE,
      'access callback' => TRUE,
    );

  return $items;
}


/**
 * Implements hook_preprocess(&$variables, $hook)
 *
 * Hooking into the preprocess and redirecting users away from the node/add/'' pages
 */
function advanced_forum_maintenance_preprocess(&$variables, $hook) {

  // globals
  global $user;
  global $base_url;

  // Settings vars
  $maintenance_enabled = variable_get('afm_enable_maintenance', 0);
  $bypass_roles        = variable_get('afm_set_role_restrictions', array(''));
  $node_types          = variable_get('afm_set_node_restrictions', array('forum'));
  $drupal_message      = t(variable_get('afm_drupal_message', ''));

  // Get a nice array of content types enabled for redirect
  // Only create an array with actual values. 0 means not selected.
  $redirect_node_types = array();
  foreach ($node_types as $key => $value) {
    if ($value != '0') {
      $redirect_node_types[] = $value;
    }
  }

  // If maintenance mode is enabled, restrict access.
  if ($maintenance_enabled == 1) {

    // If hook is page
    if ($hook == 'page') {

      // If page is node/add/
      // - proceed to check that the content type is currently
      if (arg(0) == 'node' && arg(1) == 'add') {

        // If the current node add page is a content type which has been restricted
        // use a drupal go to, to redirect the user.
        if ( in_array(arg(2), $redirect_node_types)) {

          // Access based on user role
          if ( isset($user->roles) && !array_intersect($user->roles, $bypass_roles)) {

            // Get the forum alias path - or just normal path if there is one.
            $forum_path    = drupal_get_path_alias('forum');

            // And get the referral path of the last visited page
            $current_path  = $base_url . '/' . current_path();

            // If the current path DOESNT equals the referrer path then set the referral path
            // if it does, don't set the path and it will fall back to forum homepage
            if ( $current_path != check_url($_SERVER["HTTP_REFERER"])) {
              $referrer_path = check_url($_SERVER["HTTP_REFERER"]);
            }

            // if there is a referrer path redirect back to the referred page
            if (isset($referrer_path) && drupal_valid_path($referrer_path)) {

              // Set the drupal message and redirect
              drupal_set_message(check_plain($drupal_message));
              drupal_goto($_SERVER["HTTP_REFERER"]);
            }
            // If there was no referrer then return the user to the forum homepage
            elseif (isset($forum_path)) {

              // Set the drupal message and redirect
              drupal_set_message(check_plain($drupal_message));
              drupal_goto($forum_path);
            }
          }
        }
      }
    }
  }
}


/**
 * Implements hook_node_view($node, $view_mode, $langcode)
 *
 * Hooking straight into the node view so set whether comments should be open or close.
 * We set comments to show on a node but not so that new ones can be created ($node->comment = 2)
 */
function advanced_forum_maintenance_node_view($node, $view_mode, $langcode) {

  // globals
  global $user;
  global $base_url;

  // Settings vars
  $maintenance_enabled = variable_get('afm_enable_maintenance', 0);
  $bypass_roles        = variable_get('afm_set_role_restrictions', array(''));
  $node_types          = variable_get('afm_set_node_restrictions', array('forum'));
  $drupal_message      = t(variable_get('afm_drupal_message', ''));

  // Get a nice array of content types enabled for redirect
  // Only create an array with actual values. 0 means not selected.
  $redirect_node_types = array();
  foreach ($node_types as $key => $value) {
    if ($value != '0') {
      $redirect_node_types[] = $value;
    }
  }

  // If maintenance mode is enabled, restrict access.
  if ($maintenance_enabled == 1) {

    // If the current node has a type which is being restricted - proceed
    // use a drupal go to, to redirect the user.
    if ( isset($node->type) && in_array($node->type, $redirect_node_types)) {

      // Access based on user role
      if ( isset($user->roles) && !array_intersect($user->roles, $bypass_roles)) {

        // Set the Drupal message and redirect
        drupal_set_message(check_plain($drupal_message));

        // If we're at this stage then disable the comments form.
        $node->comment = 1;

        // Return the node object
        return $node;
      }
    }
  }
}
