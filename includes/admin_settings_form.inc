<?php
/**
 * @file
 * Advanced forum maintenance admin settings form
 */


/**
 * Implements hook_form().
 *
 * Admin configuration form.
 */
function advanced_forum_maintenance_admin_form() {

  // The main group
  $form['afm_settings'] = array(
    '#type' => 'vertical_tabs',
  );

    $form['afm_maintenance_mode'] = array(
      '#type'  => 'fieldset',
      '#title' => t('Enable maintenance'),
      '#group' => 'afm_settings',
    );

      $form['afm_maintenance_mode']['afm_enable_maintenance'] = array(
        '#title'         => t('Enable maintenance mode'),
        '#description'   => t('Enabling this setting will block all users from posting on the form or create new threads.'),
        '#type'          => 'checkbox',
        '#default_value' => variable_get('afm_enable_maintenance', 0),
      );

      $form['afm_maintenance_mode']['afm_drupal_message'] = array(
        '#title'         => t('Drupal message to show'),
        '#description'   => t('This message is disabled after being redirected.'),
        '#type'          => 'textarea',
        '#default_value' => variable_get('afm_drupal_message', 'Our forum is currently undergoing maintenance. Posting has been disabled temporarily.'),
      );


    $form['afm_role_restrictions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Role restrictions'),
      '#group' => 'afm_settings',
    );

    // Get all user roles
    $all_user_roles = user_roles();

      $form['afm_role_restrictions']['afm_set_role_restrictions'] = array(
        '#type'          => 'checkboxes',
        '#options'       => drupal_map_assoc($all_user_roles),
        '#title'         => t('Bypass maintenance mode'),
        '#description'   => t('The roles which are selected will bypass the maintenance mode.'),
        '#default_value' => variable_get('afm_set_role_restrictions', array('')),
      );

    $form['afm_node_restrictions'] = array(
      '#type' => 'fieldset',
      '#title' => t('Node restrictions'),
      '#group' => 'afm_settings',
    );

    // Get all content types
    $all_content_types = node_type_get_types();

    // make a simple array - ready for use with drupal_map_assoc
    foreach ($all_content_types as $type => $info) {
      $all_content_types_flat[] = $type;
    }
    // We want these in alphabetical order please.
    asort($all_content_types_flat);

      $form['afm_node_restrictions']['afm_set_node_restrictions'] = array(
        '#type'          => 'checkboxes',
        '#options'       => drupal_map_assoc($all_content_types_flat),
        '#title'         => t('Block creation of nodes and comments on content types'),
        '#description'   => t('Disable creating new content and comments for the content types selected.'),
        '#default_value' => variable_get('afm_set_node_restrictions', array('forum')),
      );

  return system_settings_form($form);
}
