-- SUMMARY --

  A module to handle access to the advanced forum - forum maintenance

For a full description of the module, visit the project page
  http://drupal.org/project/

To submit bug reports and feature suggestions, or to track changes
  http://drupal.org/project/issues/


-- REQUIREMENTS --

Advanced forum.


-- INSTALLATION --

 Install as usual, see http://drupal.org/node/70151 for further information.


 -- CONFIGURATION --

 Configure which content types and roles content entry will apply to 

  - admin/config/development/forum-maintenance


-- CONTACT --

Current maintainers
 Jez C - https://www.drupal.org/user/2416216

 
This project has been sponsored by
 Precedent.
	Whether we're defining organisation-wide digital strategies, 
	crafting amazing web and mobile experiences, building impactful brands, 
	or creating persuasive campaigns, we transform organisations through digital, 
	and we've been doing it for more than twenty years.
	See more at: http://precedent.com/what-we-do